"""
用户管理 只有 展示用户 和 新增用户

主要新增用户可以帮助我们添加测试账号
"""
#1 APIView               一级视图 基本上没有什么封装
#2 GenericAPIView        二级视图 和mixin配合使用一般要设置 queryset和serializer_class
#3 ListAPIView           三级视图  get方法不用写 queryset和serializer_class
#4 ModelViewSet          四级视图   增删改查全都有

from rest_framework.mixins import ListModelMixin
from rest_framework.generics import ListAPIView
from apps.users.models import User
from apps.meiduo_admin.serializers.user import UserModelSerializer
from apps.meiduo_admin.utils import PageNum
from rest_framework.generics import CreateAPIView

# ListCreateAPIView 功能 = ListAPIView 功能 + CreateAPIView 功能
from rest_framework.generics import ListCreateAPIView
class UserListAPIView(ListCreateAPIView):
    """
    http://127.0.0.1:8000/meiduo_admin/users/?page=1&pagesize=10&keyword=itcast
    #获取查询字符串keyword数据
    keyword=self.request.query_params.get('key')
    #进行模拟查询
    User.objects.filter(username__contains=keyword)
    #不进行模糊查询的时候
    User.objects.all()
    """
    #设置查询结果集
    # queryset =  User.objects.all()
    # queryset = User.objects.filter(username__contains=keyword)

    def get_queryset(self):
        # 获取keyword 根据keyword进行判断
        keyword=self.request.query_params.get('keyword')
        # 如果keyword有值,则进行模糊查询
        if keyword:
            return User.objects.filter(username__contains=keyword)
        else:
            # 没有值,则获取所有用户信息
            return User.objects.all()



    #设置序列化器
    serializer_class = UserModelSerializer

    #设置分页
    pagination_class = PageNum