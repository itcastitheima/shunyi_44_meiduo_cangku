from rest_framework.viewsets import ModelViewSet
from apps.goods.models import SKU
from apps.meiduo_admin.serializers.sku import SKUModelSerializer
from apps.meiduo_admin.utils import PageNum

from rest_framework.permissions import DjangoModelPermissions

class SKUModelViewSet(ModelViewSet):

    queryset = SKU.objects.all()

    serializer_class = SKUModelSerializer

    pagination_class = PageNum

    # 权限
    permission_classes = [
        # 对于模型的 增删改查的权限 就添加上了
        DjangoModelPermissions
    ]


##############################################################
"""
获取三级分类数据
方式1: 目前可以.如果我们的数据一旦发生变化,就有问题了

parent_id 为None 的表示          1级分类 （相当于省）     id (1~37)
parent_id 为 （1~37）的表示       2级分类 （相当于市）     id (38~114)
parent_id 为 （38~114）的表示     3级分类 （相当于区）     id (115~)

GoodsCategory.objects.filter(parent_id__gt=37,lte=114)

方式2:
GoodsCategory.objects.filter(subs=None)

"""
from apps.goods.models import GoodsCategory
from rest_framework.views import APIView
from apps.meiduo_admin.serializers.sku import GoodsCategoryModelSerializer
from rest_framework.response import Response

class GoodsCategoryAPIView(APIView):

    def get(self,request):
        """
        1. 获取查询结果集
        2. 创建序列化器,传递查询结果集
        3. 返回响应
        :param request:
        :return:
        """
        # 1. 获取查询结果集
        gcs=GoodsCategory.objects.filter(subs=None)
        # 2. 创建序列化器,传递查询结果集
        s = GoodsCategoryModelSerializer(instance=gcs,many=True)
        # 3. 返回响应
        return Response(s.data)

#########################################################
# 获取所有SPU
from apps.goods.models import SPU
from rest_framework.generics import ListAPIView
from apps.meiduo_admin.serializers.sku import SimpleSPUModelSerializer

class SPUSimpleListAPIView(ListAPIView):

    queryset = SPU.objects.all()

    serializer_class = SimpleSPUModelSerializer

#########################################################
# - 表和表之间的关系
#       SPU             规格                      规格选项
#   - tb_spu --> tb_spu_specification --> tb_specification_option
# - 模型数据的获取
#   - SPU --> SPUSpecification  --> SpecificationOption

from apps.goods.models import SPUSpecification    #spu规格
from apps.goods.models import SpecificationOption   # 规格选项
from apps.meiduo_admin.serializers.sku import SPUSpecModelSerializer
class GoodsSpecsAPIView(APIView):


    def get(self,request,spu_id):

        # 1. 根据 spu_id 查询 规格信息
        # 规格信息有n个
        specs=SPUSpecification.objects.filter(spu_id=spu_id)
        # specs = [SPUSpecification,SPUSpecification]

        # 2. 创建序列化器实例,传递查询结果集
        s = SPUSpecModelSerializer(instance=specs,many=True)
        # 3. 返回响应
        return Response(s.data)

