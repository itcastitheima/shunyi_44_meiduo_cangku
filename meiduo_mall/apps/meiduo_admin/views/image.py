from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet
from apps.goods.models import SKUImage
from apps.meiduo_admin.serializers.image import SKUImageModelSerializer
from apps.meiduo_admin.utils import  PageNum

# 对于图片的操作 是增删改查
class ImageModelViewSet(ModelViewSet):

    #查询结果集
    queryset = SKUImage.objects.all()
    # 序列化器
    serializer_class = SKUImageModelSerializer
    #分页
    pagination_class = PageNum

    # 系统的方法 不能帮助我们实现七牛云图片上传,
    # 所以重写create方法,我们自己上传
    def create(self, request, *args, **kwargs):
        """
        1.接收数据
        2.提取数据
        3.验证数据
        4.图片上传七牛云,我们把七牛云给我们返回的图片名字保存到数据库
        5.数据入库
        6.返回响应
        """
        # FILES：一个类似于字典的对象，包含所有的上传文件
        # -- 图片数据是保存在 request.FILES里
        upload_image=request.FILES.get('image')

        # 1.接收数据
        # sku 在requst.data
        data=request.data
        # 2.提取数据
        sku_id=data.get('sku')

        # 3.验证数据
        try:
            sku = SKU.objects.get(id=sku_id)
        except SKU.DoesNotExist:
            return Response({'msg':'没有此商品'})
        # 4.图片上传七牛云,我们把七牛云给我们返回的图片名字保存到数据库
        from qiniu import Auth, put_data, etag
        # 需要填写你的 Access Key 和 Secret Key
        access_key = 'q9crPZPROOXrykaH85q_zpEEll0f_LsjXwUnXHRo'
        secret_key = 'lG_4_tI8bJTR8Zk6z8fGwYp79aQHkJgolvvBL_qm'
        # 构建鉴权对象
        q = Auth(access_key, secret_key)
        # 要上传的空间
        bucket_name = 'shunyi44'

        """
        # 上传后保存的文件名
        key = 'my-python-logo.png'
        # 生成上传 Token，可以指定过期时间等
        token = q.upload_token(bucket_name, key, 3600)
        # 要上传文件的本地路径
        localfile = './sync/bbb.jpg'
        ret, info = put_file(token, key, localfile)
        
        以上4行代码 是进行本地图片上传
        我们其实是在进行二进制图片的上传
        """
        # 上传后保存的文件名--使用系统,我们不使用
        key = None
        # 生成上传 Token，可以指定过期时间等
        token = q.upload_token(bucket_name, key, 3600)
        # 要上传二进制数据
        # 读取图片的二进制
        data=upload_image.read()

        # ret 结果
        # info 上传信息
        ret, info = put_data(token, key, data=data)

        # ret['key']是自动生成的图片名字
        image_url = ret['key']

        # 5.数据入库
        new_image=SKUImage.objects.create(
            sku_id=sku_id,
            image=image_url
        )
        # 6.返回响应
        # 一定要返回 201 因为前端就是根据 201 来判断的
        return Response({'id':new_image.id,
                         'image':new_image.image.url,
                         'sku':sku_id
                         },status=201)

    def update(self, request, *args, **kwargs):
        """
        1. 接收数据
        2. 提取数据
        3. 验证数据
        4. 七牛云上传新图片
        5. 数据更新
        6. 返回响应
        :param request:
        :param args:
        :param kwargs:
        :return:
        """
        # 1. 接收数据
        data=request.data

        # 2. 提取数据
        sku_id=data.get('sku')
        new_upload_image=data.get('image')

        # 3. 验证数据 (省略)

        # 4. 七牛云上传新图片
        # 4.图片上传七牛云,我们把七牛云给我们返回的图片名字保存到数据库
        from qiniu import Auth, put_data
        # 需要填写你的 Access Key 和 Secret Key
        access_key = 'q9crPZPROOXrykaH85q_zpEEll0f_LsjXwUnXHRo'
        secret_key = 'lG_4_tI8bJTR8Zk6z8fGwYp79aQHkJgolvvBL_qm'
        # 构建鉴权对象
        q = Auth(access_key, secret_key)
        # 要上传的空间
        bucket_name = 'shunyi44'

        # 上传后保存的文件名--使用系统,我们不使用用户上传的名字
        key = None
        # 生成上传 Token，可以指定过期时间等
        token = q.upload_token(bucket_name, key, 3600)
        # 要上传二进制数据
        # 读取图片的二进制
        data = new_upload_image.read()

        # ret 结果
        # info 上传信息
        ret, info = put_data(token, key, data=data)

        # ret['key']是自动生成的图片名字
        new_image_url = ret['key']

        # 5. 数据更新
        # SKUImage.objects.filter(sku_id=sku_id).update()   返回影响的条数 不是返回对象
        #
        pk = self.kwargs.get('pk')
        new_image = SKUImage.objects.get(id=pk)

        new_image.image=new_image_url
        new_image.save()
        # 6. 返回响应
        return Response({
            'id':new_image.id,
            'image':new_image.image.url,
            'sku':sku_id
        })

#'FvfgHS81J8Rte3nzawhvVZlTAE5r'
#'Fjh-qaNsFoAvw_1Ns4YNPktwGvX6'
# 获取所有SKU数据
from rest_framework.generics import ListAPIView
from apps.goods.models import SKU
from apps.meiduo_admin.serializers.image import SimpleSKUModelSerializer
class SimpleSKUListAPIView(ListAPIView):

    queryset = SKU.objects.all()

    serializer_class = SimpleSKUModelSerializer