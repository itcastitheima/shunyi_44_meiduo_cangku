
from django.contrib.auth.models import Permission
from django.contrib.auth.models import ContentType
"""
之前讲的权限 是 是否可以登录后台系统上

登录后台系统的用户 又有权限 

所谓的权限 其实 还是会体现在 对于模型的增删改查上

"""

from rest_framework.viewsets import ModelViewSet
from django.contrib.auth.models import Permission
from apps.meiduo_admin.serializers.permission import PermissionModelSerializer
from apps.meiduo_admin.utils import  PageNum

class PermissionModelViewSet(ModelViewSet):
    #查询所有的权限
    queryset = Permission.objects.all()

    serializer_class = PermissionModelSerializer
    #设置分页
    pagination_class = PageNum


#######################################
# 内容类型
from django.contrib.auth.models import ContentType

from rest_framework.generics import ListAPIView
from apps.meiduo_admin.serializers.permission import ContentTypeModelSerializer

class ContentTypeListAPIView(ListAPIView):
    # 查询所有数据
    queryset = ContentType.objects.all()

    serializer_class = ContentTypeModelSerializer
