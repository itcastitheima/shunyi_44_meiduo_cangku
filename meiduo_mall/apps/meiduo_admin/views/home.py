
"""
GET meiduo_admin/statistical/day_active/

获取 当天的登录用户是数量

last_login 数据库的数据是 datetime【2021-1-27 10:05:05】

今天 【2021-1-27】  date    --> datetime 【2021-1-27 00:00:00】

"""
from apps.users.models import User
from datetime import date
# filter(属性名称__比较运算符=值)
# 例如: filter(id__exact=1)

# 代码是应该写入类视图的
#1 APIView               一级视图 基本上没有什么封装
#2 GenericAPIView        二级视图 和mixin配合使用一般要设置 queryset和serializer_class
#3 ListAPIView           三级视图  get方法不用写 queryset和serializer_class
#4 ModelViewSet          四级视图   增删改查全都有
from rest_framework.views import APIView
from rest_framework.response import Response
class UserActiveAPIView(APIView):

    def get(self,request):
        # 1. 获取今天的date
        today = date.today()
        # 2. 过滤查询
        # count() 获取数量
        count = User.objects.filter(last_login__gte=today).count()

        return Response({'count':count})

#查询图书，要求图书中人物的描述包含"八"
#BookInfo.objects.filter(peopleinfo__description__contains='八')

# 下单用户
class UserOrderAPIView(APIView):
    def get(self,request):

        today=date.today()

        count=User.objects.filter(orderinfo__create_time__gte=today).count()

        return Response({'count':count})



"""
最终实现的是 月增用户统计
统计每一天新注册的用户

date_joined 数据库的数据是 datetime【2021-1-27 10:05:05】

某一天 【2021-1-27】  date    --> datetime 【2021-1-27 00:00:00】


1. 返回的数据形式
[
        {
            "count": "用户量",
            "date": "日期"
        },
        {
            "count": "用户量",
            "date": "日期"
        },
        ...
]

2. 
    我们先获取今天的日期,再获取30天前的日期
    遍历获取每一天的新增用户数量
    要获取一个日期的范围.大于等于某一个,小于它的下一天

"""
from datetime import timedelta
class MonthUserAPIView(APIView):

    def get(self,request):
        # 我们先获取今天的日期, 再获取30天前的日期
        today = date.today()
        # 获取30天前的日期
        before_date = today - timedelta(days=30)
        #初始化一个字典列表
        data_list=[]
        # 遍历获取每一天的新增用户数量
        for i in range(0,30):
            # 从30天前开始 累加天数
            # 范围开始的日期
            start_date = before_date + timedelta(days=i)
            # 范围结束的日期
            end_date = before_date + timedelta(days=(i+1))
            # 要获取一个日期的范围.大于等于某一个,小于它的下一天
            count = User.objects.filter(date_joined__gte=start_date,date_joined__lt=end_date).count()
            # 追加到列表中
            data_list.append({
                'count':count,
                'date':start_date
            })
        return Response(data_list)
