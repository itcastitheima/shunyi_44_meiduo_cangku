# 不要使用
# from django.contrib.auth.models import User

# 这个是我们使用的 User
from apps.meiduo_admin.serializers.admin import UserModelSerializer
from apps.meiduo_admin.utils import PageNum
from apps.users.models import User

from rest_framework.viewsets import ModelViewSet

class AdminModelViewSet(ModelViewSet):
    # 我们查询 is_staff = 1
    # is_super=1 那么 is_staff 也是1
    queryset = User.objects.filter(is_staff=1)

    serializer_class = UserModelSerializer

    pagination_class = PageNum


from rest_framework.generics import ListAPIView
from django.contrib.auth.models import Group
from apps.meiduo_admin.serializers.admin import GroupModelSerialzier

class GroupListAPIView(ListAPIView):

    queryset = Group.objects.all()

    serializer_class = GroupModelSerialzier