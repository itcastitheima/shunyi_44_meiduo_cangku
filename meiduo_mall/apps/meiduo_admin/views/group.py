from django.contrib.auth.models import Group
from rest_framework.viewsets import ModelViewSet
from apps.meiduo_admin.serializers.group import GroupModelSerializer
from apps.meiduo_admin.utils import PageNum


class GroupModelViewSet(ModelViewSet):

    queryset = Group.objects.all()

    serializer_class = GroupModelSerializer

    pagination_class = PageNum


from rest_framework.generics import ListAPIView
from django.contrib.auth.models import Permission
from apps.meiduo_admin.serializers.permission import PermissionModelSerializer
class PermissionListAPIView(ListAPIView):

    queryset = Permission.objects.all()

    serializer_class = PermissionModelSerializer