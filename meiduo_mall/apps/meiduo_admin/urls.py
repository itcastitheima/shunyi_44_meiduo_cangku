from django.urls import path
from rest_framework_jwt.views import obtain_jwt_token, ObtainJSONWebToken
from apps.meiduo_admin.login import admin_obtain_token
from apps.meiduo_admin.views import home,user,image,sku,permission,group,admin
urlpatterns = [
    # vue的ajax请求都是固定的.我们按照文档来
    # 我们登录的请求就是: meiduo_admin/authorizations/
    # 登录就实现了!!!
    # path('authorizations/',obtain_jwt_token),
    path('authorizations/',admin_obtain_token),

    # 日活用户
    path('statistical/day_active/',home.UserActiveAPIView.as_view()),
    # 日下单用户量
    path('statistical/day_orders/',home.UserOrderAPIView.as_view()),

    # 月增用户折现图
    path('statistical/month_increment/',home.MonthUserAPIView.as_view()),

    path('users/',user.UserListAPIView.as_view()),


    # 图片中 获取sku数据
    path('skus/simple/',image.SimpleSKUListAPIView.as_view()),



    # SKU的三级分类
    path('skus/categories/',sku.GoodsCategoryAPIView.as_view()),

    # SPU的简单获取
    path('goods/simple/',sku.SPUSimpleListAPIView.as_view()),

    # 获取spu的规格和规格选项
    path('goods/<spu_id>/specs/',sku.GoodsSpecsAPIView.as_view()),


    ################################################
    # 权限中 获取所有类型
    path('permission/content_types/',permission.ContentTypeListAPIView.as_view()),

    # 获取所有的权限
    path('permission/simple/',group.PermissionListAPIView.as_view()),

    #获取所有组
    path('permission/groups/simple/',admin.GroupListAPIView.as_view()),

    # obtain_jwt_token = ObtainJSONWebToken.as_view()
    # path('authorizations/',ObtainJSONWebToken.as_view()),
]

from rest_framework.routers import DefaultRouter

# 1. 创建router
router=DefaultRouter()
# 2. 注册url
# prefix
# viewset
# basename
router.register('skus/images',image.ImageModelViewSet,basename='images')
# 3. 将router自动生成的url 追加到urlpatterns
urlpatterns += router.urls


##########SKU###############################
router.register('skus',sku.SKUModelViewSet,basename='skus')
# 3. 将router自动生成的url 追加到urlpatterns
urlpatterns += router.urls



##########权限###############################
router.register('permission/perms',permission.PermissionModelViewSet,basename='perms')
# 3. 将router自动生成的url 追加到urlpatterns
urlpatterns += router.urls

##########组###############################
router.register('permission/groups',group.GroupModelViewSet,basename='groups')
# 3. 将router自动生成的url 追加到urlpatterns
urlpatterns += router.urls

##########组###############################
router.register('permission/admins',admin.AdminModelViewSet,basename='admins')
# 3. 将router自动生成的url 追加到urlpatterns
urlpatterns += router.urls