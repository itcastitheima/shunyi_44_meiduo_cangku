from apps.goods.models import SKU,SKUSpecification
from rest_framework import serializers
"""
{
    caption: "aaaa"
    category_id: 115
    cost_price: "1"
    is_launched: "true"
    market_price: "1"
    name: "aaa"
    price: "1",
    spu_id: 2
    stock: "1"
    specs: [
    		{spec_id: "4", option_id: 8}, 
			{spec_id: "5", option_id: 11}
		]

}
"""
class SKUSpecificationModelSerializer(serializers.ModelSerializer):

    spec_id=serializers.IntegerField()
    option_id=serializers.IntegerField()

    class Meta:
        model = SKUSpecification
        fields = ['spec_id','option_id']

class SKUModelSerializer(serializers.ModelSerializer):

    # category_id 对应 外键值
    category_id=serializers.IntegerField()
    # category 对应 模型的 __str__ 返回的数据
    category=serializers.StringRelatedField()

    spu_id=serializers.IntegerField()
    spu=serializers.StringRelatedField()

    specs = SKUSpecificationModelSerializer(many=True)

    class Meta:
        model=SKU
        fields='__all__'        # 偷懒的做法
        # fields = ['id','name','caption']
        # exclude = []

    def create(self, validated_data):

        # 1. 把1对多的 多的数据 pop出来,单独用一个变量接收
        specs = validated_data.pop('specs')

        from django.db import transaction

        #  with transaction.atomic(): 事务 是可以自动进行 事务的提交和回滚的.可以不用异常捕获[意思是 异常自动回滚]
        with transaction.atomic():
            save_point=transaction.savepoint()
            try:
                # 2. validated_data 就剩下 sku的数据了
                sku = SKU.objects.create(**validated_data)
                # 3. 对多的数据进行遍历保存
                for spec in specs:
                    # spec = {spec_id: "4", option_id: 8}
                    SKUSpecification.objects.create(sku=sku,**spec)
            except Exception:
                transaction.savepoint_rollback(save_point)

            else:
                transaction.savepoint_commit(save_point)

        return sku


    def update(self, instance, validated_data):
        # 1. 把 1对多 的 多的数据 pop出来
        specs = validated_data.pop('specs')
        # 2. validated_data 数据就只剩下 sku的数据了
        super().update(instance,validated_data)
        # 3. 遍历 多的 数据,进行更新
        for spec in specs:
            # spec = {"spec_id": "规格id", "option_id": "选项id"}
            # instance 就是 具体的sku
            # 我们不支持修改 规格. 查询的时候也要查询对应的规格
            SKUSpecification.objects.filter(sku=instance,spec_id=spec.get('spec_id')).update(option_id=spec.get('option_id'))

        return instance



#####三级分类序列化器########################################

from apps.goods.models import GoodsCategory

class GoodsCategoryModelSerializer(serializers.ModelSerializer):

    class Meta:
        model=GoodsCategory
        fields=['id','name']


######Simple SPU#####################################
from apps.goods.models import SPU
class SimpleSPUModelSerializer(serializers.ModelSerializer):

    class Meta:
        model = SPU
        fields = ['id','name']

#######################################################



from apps.goods.models import SpecificationOption
class SpecificationOptionModelSerializer(serializers.ModelSerializer):

    class Meta:
        model = SpecificationOption
        fields = ['id','value']



from apps.goods.models import SPUSpecification

class SPUSpecModelSerializer(serializers.ModelSerializer):

    spu=serializers.StringRelatedField()
    spu_id=serializers.IntegerField()

    options = SpecificationOptionModelSerializer(many=True)

    class Meta:
        model = SPUSpecification
        fields = ['id','name','spu','spu_id','options']





