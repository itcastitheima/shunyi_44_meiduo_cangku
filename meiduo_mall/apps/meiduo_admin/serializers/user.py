from rest_framework import serializers
from apps.users.models import User
# 有模型
class UserModelSerializer(serializers.ModelSerializer):

    class Meta:
        model = User                                    # 指定模型类
        fields = ['id','username','mobile','email','password']     # 指定字段
        # 设置字段的选项信息
        extra_kwargs = {
            'password': {
                'write_only':True,
                'max_length': 20,
                'min_length': 5
            }
        }

    def create(self, validated_data):
        #重写方法
        return User.objects.create_user(**validated_data)