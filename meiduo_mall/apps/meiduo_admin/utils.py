def jwt_response_payload_handler(token, user=None, request=None):
    # 控制返回响应的数据方法
    #token,             系统生成的token
    # user=None,        登录认证的user
    # request=None      请求
    return {
        'token': token,
        'username':user.username,
        'user_id':user.id
    }


from rest_framework.pagination import PageNumberPagination
from rest_framework.response import Response
class PageNum(PageNumberPagination):
    # 设置默认的一页多少条数据
    # 必须要设置,设置了它,就相当于开始了分页使用这个分页类
    page_size = 2

    # 设置前端的请求参数
    # http://api.example.org/accounts/?page=4&page_size=100
    # key=value
    # key
    page_size_query_param = 'pagesize'
    # 重写系统的方法
    def get_paginated_response(self, data):

        return Response({
            'count':self.page.paginator.count,   #总共有多少条记录
            'lists':data,                        #数据
            'page':self.page.number,             #第几页
            'pages':self.page.paginator.num_pages, # 总共多少页
            'pagesize':self.page_size,  #每页多少条数据
        })

